import cv2
import numpy as np
import urllib.request

stream = urllib.request.urlopen('http://127.0.0.1:5000/video_feed')
# b'' 表示內容為bytes的格式
bytes= b''
while True:
    # 讀取固定長度的影像資訊
    bytes += stream.read(1024)
    # 查詢Base64影像的開頭
    a = bytes.find(b'\xff\xd8')  # JPEG start
    # 查詢Base64影像的結尾
    b = bytes.find(b'\xff\xd9')  # JPEG end

    # 若可發現開頭及結尾便將內容擷取出來
    if a!=-1 and b!=-1:
        # 擷取範圍內的資料
        jpg = bytes[a:b+2] # actual image
        # 將擷取的內容從原資料割除
        bytes= bytes[b+2:] # other informations

        # 將Base64 轉為 numpy.arrar的資料格式
        img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR)

        cv2.imshow('Window name',img) # display image while receiving data
        if cv2.waitKey(1) =='q': # if user hit esc
            exit(0) # exit program